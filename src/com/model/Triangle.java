package com.model;

public class Triangle
{
	private String name;

	public String getName() {		
		System.out.println("Getter Method of Triangle class called");		
		
		return name;
	}

	public void setName(String name) {
		this.name = name;
		System.out.println("Setter Method of Triangle is called");
	}
	
	public String setNameAndReturn(String name)
	{
		this.name=name;
		System.out.println("Triangle set Name and Return");
		return name;
	}
}
