package com.service;

import com.aspect.Loggable;
import com.model.Circle;
import com.model.Triangle;

public class ShapeService 
{
	private Circle circle;
	private Triangle triangle;
	
	public Circle getCircle() {
		return circle;
	}
	public void setCircle(Circle circle) {
		this.circle = circle;
	}
	@Loggable
	public Triangle getTriangle() {
		System.out.println("getTriangle Called");
		return triangle;
	}
	public void setTriangle(Triangle triangle) {
		this.triangle = triangle;
	}
	
}
