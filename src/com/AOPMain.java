package com;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.service.ShapeService;

public class AOPMain {

	public static void main(String[] args) 
	{
		ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");		
		
		ShapeService shapeService = context.getBean("shapeService", ShapeService.class);						
		System.out.println(shapeService.getTriangle().getName());
		
		/*Triangle triangle = context.getBean("triangle", Triangle.class);						
		triangle.setName("Dummy");*/
		//triangle.setNameAndReturn("Dummy");
	}

}
