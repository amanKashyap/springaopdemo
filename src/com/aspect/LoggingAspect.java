package com.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Aspect
public class LoggingAspect 
{
	@AfterThrowing("@annotation(com.aspect.Loggable)")
	public void loggingAdvice()
	{
		System.out.println("Advice Run.");
	}
	
	@Pointcut("execution(* get*(..))")		
	public void allGetter()
	{ }
	
	@Pointcut("within(com.model.Triangle)")
	public void allTriangleMethods() {} 
	
	//Apply to all methods containing a single String argument.
	@Before("args(name)")
	public void singleStringArg(String name)
	{
		System.out.println("Name: "+name);		
	}
	
	@Around("allTriangleMethods()")
	public Object myAroundAdvice(ProceedingJoinPoint procedingJoinPoint) throws Throwable
	{
		System.out.println("Before the Target Method Executes");				
		Object returnValue = procedingJoinPoint.proceed();
		Object[] joinPointArgs=procedingJoinPoint.getArgs();
		
		System.out.println("After Target Method");
		
		return returnValue;
	}
	
}	
